import numpy as np


def run_model(x0, n, seed):
    """
    A simple model :math:`x_0 + X: X \\sim U(0, 1)`.

    :param x0: The model parameter :math:`x_0`.
    :param n: The number of samples to return.
    :param seed: The seed for the random number generator.
    """
    if seed is None:
        raise ValueError("RNG seed is undefined")
    rng = np.random.default_rng(seed=seed)
    return x0 + rng.random(size=n)
