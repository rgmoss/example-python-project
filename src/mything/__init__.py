import copy
from dataclasses import dataclass
import numpy as np
import tomli
from typing import Any, Dict

from . import model


@dataclass
class Scenario:
    """
    The definition of a single model scenario.

    :param name: A name that briefly describes the scenario.
    :param settings: The model settings for this scenario.
    :param extra: Additional information from the scenario file.
    """

    name: str
    settings: Dict[str, Any]
    extra: Dict[str, Any]


def run_scenario(scenario):
    """
    Run a single model scenario and return the results.

    :param scenario: A model scenario.
    :type scenario: Scenario
    """
    parameters = scenario.settings["parameters"]
    x0 = parameters["x0"]
    n = parameters["num_samples"]
    seed = parameters["rng_seed"]
    return model.run_model(x0, n, seed)


def run_scenario_many_times(scenario):
    """
    Run a single scenario many times with different seeds and return a matrix
    that contains all of the results.

    .. note:: The scenario must define the settings "number_of_simulations"
       and "starting_seed", in addition to the model parameters.

    :param scenario: A model scenario.
    :type scenario: Scenario
    """
    sim_count = scenario.settings["number_of_simulations"]
    starting_seed = scenario.settings["starting_seed"]

    all_results = []
    for i in range(sim_count):
        # Change the seed and run the scenario.
        scenario.settings["parameters"]["rng_seed"] = starting_seed + i
        sim_results = run_scenario(scenario)

        # Check that the results differ from the previous simulations.
        for prev_results in all_results:
            if np.array_equal(prev_results, sim_results):
                raise ValueError("Identical outputs for different seeds")

        # Add these to our list of simulation results.
        all_results.append(sim_results)

    # Collect all of the results into a 2D array.
    # This is possible because they all have the same length.
    results_matrix = np.array(all_results)

    return results_matrix


def load_scenarios(toml_file):
    """
    Load model scenarios from a TOML file.

    :param toml_file: The filename from which to read the scenarios.
    :returns: A dictionary that maps scenario names to scenarios.
    """
    with open(toml_file, "rb") as f:
        data = tomli.load(f)

    def copy_extra_info():
        return {
            key: copy.deepcopy(value)
            for key, value in data.items()
            if key != "scenarios"
        }

    scenarios = {
        name: Scenario(
            name=name,
            settings=settings,
            extra=copy_extra_info(),
        )
        for name, settings in data["scenarios"].items()
    }

    return scenarios


def load_scenarios_from_files(scenario_files, scenario_names=None):
    """
    Load model scenarios from one or more TOML files.

    :param scenario_files: The file(s) from which to read the scenarios.
    :param scenario_names: The specific scenarios to run (optional).
    :returns: A list of ``(name, scenario)`` tuples.
    """
    all_scenarios = []
    for scenario_file in scenario_files:
        # Load the scenarios from each file in turn.
        scenarios = load_scenarios(scenario_file)

        # Check whether to retain only scenarios with specific names.
        if scenario_names:
            scenarios = {
                name: scenario
                for name, scenario in scenarios.items()
                if name in scenario_names
            }

        all_scenarios.extend(scenarios.items())

    return all_scenarios
