import argparse
import h5py
from pathlib import Path

from . import load_scenarios_from_files, run_scenario_many_times


def main(args=None):
    """
    Run scenarios many times with different seeds.
    """
    parser = argument_parser()
    args = parser.parse_args(args)

    scenarios = load_scenarios_from_files(
        args.scenario_files, args.scenario_names
    )

    for name, scenario in scenarios:
        results_matrix = run_scenario_many_times(scenario)

        # Ensure that the output directory exists.
        output_dir = Path(scenario.settings["output_dir"])
        output_dir.mkdir(parents=True, exist_ok=True)

        # Save the outputs as an HDF5 dataset.
        output_file = output_dir / f"{name}.hdf5"
        print(f"Saving results to {output_file} ...")
        with h5py.File(output_file, "w") as f:
            f.create_dataset("output/raw_results", data=results_matrix)

    return 0


def argument_parser():
    """
    Return the command-line argument parser for this script.
    """
    p = argparse.ArgumentParser(description="Run model scenarios")

    # Require the user to provide one or more scenario files.
    p.add_argument("scenario_files", nargs="+", help="Scenario file(s)")

    # Allow the user to specify a subset of the scenarios.
    p.add_argument(
        "-s",
        "--scenario",
        dest="scenario_names",
        metavar="NAME",
        action="append",
        help="Select scenario(s) by name",
    )

    return p


main()
