import nox
from pathlib import Path


@nox.session(reuse_venv=True)
def scenarios(session):
    """A virtual environment for running scenarios."""
    # NOTE: install the package in editable mode.
    session.install("-e", ".")
    # Run the __main__ module if any arguments were passed to this session.
    if session.posargs:
        session.run("python3", "-m", "mything", *session.posargs)


@nox.session(reuse_venv=True)
def tests(session):
    """Run test cases and record the test coverage."""
    session.install(".[tests]")
    session.run("pytest", "--cov=mything", "--pyargs", "./tests")
    # Ensure that regression test outputs have not changed.
    session.run(
        "git", "diff", "--exit-code", "--stat", "tests/", external=True
    )


@nox.session(reuse_venv=True, tags=["check"])
def ruff(session):
    """Check code for linter warnings."""
    session.install("ruff ~= 0.1.3")
    # NOTE: also check Python files in the repository root directory.
    extra_py_files = list(Path(".").glob("*.py"))
    session.run("ruff", "src", "tests", *extra_py_files)
    session.run("ruff", "format", "--diff", "src", "tests", *extra_py_files)
