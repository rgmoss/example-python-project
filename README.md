# Example Python project

![tests status](https://gitlab.unimelb.edu.au/rgmoss/example-python-project/badges/master/pipeline.svg)
![tests coverage](https://gitlab.unimelb.edu.au/rgmoss/example-python-project/badges/master/coverage.svg)

This repository demonstrates one way of structuring a Python project.

Key features of this project are:

- The core functionality — a simple simulation model — is defined in a Python package ("mything"), which is located in `src/mything`.

- Package dependencies and other metadata are defined in [pyproject.toml](pyproject.toml).

- Parameter values are collected into "scenarios", and can be defined in plain-text configuration files.
  See [the example scenarios file](tests/example_scenarios.toml).
  Here, we're using the [TOML](https://toml.io/) file format to define scenarios, and the [tomli](https://github.com/hukkin/tomli) package to read these files.

- We use a [Data Class](https://docs.python.org/3/library/dataclasses.html) to store the details of each scenario.
  This is a convenient way to structure and document the information that we want to associate with each scenario.

- Test cases are defined in the `tests/` directory.
  Model outputs for specific scenarios (`tests/scenario_NAME.out`) are tracked by git, so that we can make the tests fail if an output changes.

- We use the [nox](https://nox.thea.codes/) automation tool to perform the following tasks (as defined in [noxfile.py](noxfile.py)):

  - Run test cases with [pytest](https://docs.pytest.org/) and report test coverage;

  - Check our code for potential issues with [ruff](https://github.com/astral-sh/ruff); and

  - Ensure consistent code formatting with [ruff](https://github.com/astral-sh/ruff).

- We define a GitLab CI pipeline in [.gitlab-ci.yml](.gitlab-ci.yml), that runs `nox` using Python 3.8 and Python 3.10, and records the test coverage.
  We can then display the test results and coverage as "badges" at the top of `README.md`.

## How to run scenarios

The `nox` session "scenarios" (defined in [noxfile.py](noxfile.py)) creates a virtual environment and installs the "mything" package and all of its dependencies.
We can use this virtual environment to run arbitrary scenarios, such as those defined in [scenarios/my-scenarios.toml](scenarios/my-scenarios.toml).

The "mything" packages includes a [\_\_main\_\_.py](src/mything/__main__.py) script that accepts one or more scenario files as command-line arguments, and runs each scenario many times.

To run this script, you must first create and activate the "sessions" virtual environment:

```sh
nox -s scenarios
. .nox/scenarios/bin/activate
```

Once you're in a virtual environment where the "mything" package has been installed, you can run the `__main__.py` script with the following command:

```sh
python3 -m mything scenarios/my-scenarios.toml
```

This will run the "example" scenario **50 times** — note that this number is defined in the scenario itself, **not** in the script — and save all of the results to `outputs/my-scenarios/example.hdf5`.

As a shortcut, the "scenarios" session will run the `__main__.py` script if you pass any command-line arguments (after a `--` argument).
For example, to run the scenarios defined in `scenarios/my-scenarios.toml`:

```sh
nox -s scenarios -- scenarios/my-scenarios.toml
```

## Some suggestions

- Consider using an editor that will automatically format your code with [ruff](https://github.com/astral-sh/ruff) and display linter messages.
  This will ensure that the "ruff" session will always succeed in the CI pipeline.
  It's also a good habit in general.

- The [lhs package](https://lhs.readthedocs.io/) provides an easy way to define parameter distributions and draw samples values.
  For example, the [pypfilt package](https://pypfilt.readthedocs.io/) uses this as its default sampler, and includes [an example](https://pypfilt.readthedocs.io/en/0.8.0/getting-started/forecasts.html) of defining parameter distributions in TOML files.

- If you intend to run multiple simulations in parallel, the [parq package](https://parq.readthedocs.io/) might be a convenient solution.
  It distributes jobs over multiple processes and provides information about which jobs succeeded and which jobs failed (if any).

- Try **breaking the reproducibility test cases** by:

  - Changing the "rng_seed" parameter for one or both scenarios in `tests/example_scenarios.toml`;

  - Committing these changes:

    ```sh
    git add tests/example_scenarios.toml
    git commit -m "Change the seed to break a reproducibility test"
    ```

   - Running the test cases:

     ```sh
     nox -s tests
     ```
