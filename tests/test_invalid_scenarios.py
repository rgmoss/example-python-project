"""
Test running invalid scenarios.
"""

import mything
import pytest


def test_no_seed_run_scenario():
    """
    Check that run_scenario() fails when the "prng_seed" parameter is missing.
    """
    settings = {
        "parameters": {
            "x0": 5,
            "num_samples": 10,
        },
    }
    scenario = mything.Scenario(name="test", settings=settings, extra={})

    with pytest.raises(KeyError):
        mything.run_scenario(scenario)


def test_no_seed_run_model():
    """
    Check that run_model() fails when the "prng_seed" parameter is ``None``.
    """
    with pytest.raises(ValueError):
        mything.model.run_model(x0=5, n=10, seed=None)
