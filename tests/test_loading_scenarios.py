"""
Test loading scenarios from multiple files.
"""

import mything


def test_loading_scenarios_from_multiple_files(tmp_path):
    """
    Load scenarios from multiple files.
    """
    file_1 = tmp_path / "file_1.toml"
    file_2 = tmp_path / "file_2.toml"
    write_test_scenario_files(file_1, file_2)

    scenarios = mything.load_scenarios_from_files([file_1, file_2])

    # Check that we received the expected four scenarios.
    assert len(scenarios) == 4
    names = {name for (name, _) in scenarios}
    assert names == {"example_a", "example_b", "example_c", "example_d"}


def test_loading_scenarios_from_multiple_files_by_name(tmp_path):
    """
    Load specific scenarios by name from multiple files.
    """
    file_1 = tmp_path / "file_1.toml"
    file_2 = tmp_path / "file_2.toml"
    write_test_scenario_files(file_1, file_2)

    names = "example_b", "example_c"
    scenarios = mything.load_scenarios_from_files([file_1, file_2], names)

    # Check that we received the expected two scenarios (one from each file).
    assert len(scenarios) == 2
    names = {name for (name, _) in scenarios}
    assert names == set(names)


def test_loading_scenarios_from_duplicate_file(tmp_path):
    """
    Load scenarios from multiple files.
    """
    file_1 = tmp_path / "file_1.toml"
    file_2 = tmp_path / "file_2.toml"
    write_test_scenario_files(file_1, file_2)

    scenarios = mything.load_scenarios_from_files([file_1, file_1])

    # Check that we received the two scenarios in file_1 *twice*.
    assert len(scenarios) == 4
    names = {name for (name, _) in scenarios}
    assert names == {"example_a", "example_b"}


def write_test_scenario_files(file_1, file_2):
    """
    Define scenarios "example_a" and "example_b" in the first file, and
    scenarios "example_c" and "example_d" in the second file.
    """
    file_1.write_text(
        """
    [scenarios.example_a]
    description = "An example scenario."
    parameters.x0 = 10
    parameters.num_samples = 4
    parameters.rng_seed = 3001

    [scenarios.example_b]
    description = "Another example scenario."
    parameters.x0 = 100
    parameters.num_samples = 40
    parameters.rng_seed = 30010
    """
    )

    file_2.write_text(
        """
    [scenarios.example_c]
    description = "An example scenario."
    parameters.x0 = 1
    parameters.num_samples = 100
    parameters.rng_seed = 3001

    [scenarios.example_d]
    description = "Another example scenario."
    parameters.x0 = 2
    parameters.num_samples = 200
    parameters.rng_seed = 30010
    """
    )
