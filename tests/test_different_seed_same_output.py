"""
Test whether identical output for different seeds produces an error.
"""

import mything
import numpy as np
import pytest


def test_different_seed_same_output():
    """
    The ``run_scenario_many_times()`` function should raise a ``ValueError``
    if the model produces identical outputs for different seeds.

    We can make this happen by setting ``x0`` to the maximum possible
    floating-point value, so that adding a random value between 0 and 1 has no
    effect.
    """
    settings = {
        "number_of_simulations": 2,
        "starting_seed": 1,
        "parameters": {
            "x0": np.finfo(float).max,
            "num_samples": 5,
        },
    }
    scenario = mything.Scenario(name="test", settings=settings, extra={})

    error_message = "Identical outputs for different seeds"
    with pytest.raises(ValueError, match=error_message):
        mything.run_scenario_many_times(scenario)
