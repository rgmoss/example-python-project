"""
Test running a scenario multiple times.
"""

import mything
import numpy as np


def test_minimal_scenario_many_times():
    """
    Run a single scenario many times and ensure that the results are valid.
    """
    times = 42
    samples = 10

    settings = {
        "number_of_simulations": times,
        "starting_seed": 1000,
        "parameters": {
            "x0": 5,
            "num_samples": samples,
        },
    }
    scenario = mything.Scenario(name="test", settings=settings, extra={})
    results = mything.run_scenario_many_times(scenario)

    # Check that we have the expected number of samples.
    assert results.shape == (times, samples)

    # Check that the values all fall within the expected range.
    assert np.all(results >= 5) & np.all(results < 6)
