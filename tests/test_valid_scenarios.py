"""
Test running valid scenarios.
"""

import numpy as np
from pathlib import Path

import mything


def test_minimal_scenario():
    """
    Run a single scenario and ensure that the results are valid.
    """
    settings = {
        "parameters": {
            "x0": 5,
            "num_samples": 10,
            "rng_seed": 12345,
        },
    }
    scenario = mything.Scenario(name="test", settings=settings, extra={})
    results = mything.run_scenario(scenario)

    # Check that we have the expected number of samples.
    assert results.shape == (10,)

    # Check that the values all fall within the expected range.
    assert np.all(results >= 5) & np.all(results < 6)


def test_loading_scenarios():
    """
    Run scenarios defined in an external TOML file.
    """
    test_dir = Path("tests")
    toml_file = test_dir / "example_scenarios.toml"
    scenario_table = mything.load_scenarios(toml_file)

    # Check that this file contains two scenarios.
    assert len(scenario_table) == 2

    for name, scenario in scenario_table.items():
        results = mything.run_scenario(scenario)

        # Check that the outputs are as expected.
        parameters = scenario.settings["parameters"]
        assert results.shape == (parameters["num_samples"],)
        assert np.all(results >= parameters["x0"])
        assert np.all(results < parameters["x0"] + 1)

        # Save the outputs to disk.
        np.savetxt(test_dir / f"scenario_{name}.out", results)
