"""
Test loading extra information from scenario files.
"""

import mything


def test_loading_extra_info_from_file(tmp_path):
    scenario_file = tmp_path / "scenarios.toml"
    scenario_file.write_text(
        """
    [metadata]
    description = "This should be included in the extra information"

    [scenarios.example_a]
    description = "An example scenario."
    parameters.x0 = 10
    parameters.num_samples = 4
    parameters.rng_seed = 3001

    [scenarios.example_b]
    description = "Another example scenario."
    parameters.x0 = 100
    parameters.num_samples = 40
    parameters.rng_seed = 30010
    """
    )

    scenarios = mything.load_scenarios(scenario_file)

    # Check that we obtain the expected scenarios.
    assert len(scenarios) == 2
    assert "example_a" in scenarios
    assert "example_b" in scenarios
    example_a = scenarios["example_a"]
    example_b = scenarios["example_b"]

    # Check that each scenario has the expected additional information.
    descr = "This should be included in the extra information"
    assert "metadata" in example_a.extra
    assert example_a.extra["metadata"]["description"] == descr
    assert "metadata" in example_b.extra
    assert example_b.extra["metadata"]["description"] == descr

    # Check that each scenario has a separate copy of this information.
    example_a.extra["metadata"]["description"] = 1234
    assert example_b.extra["metadata"]["description"] == descr
    del example_b.extra["metadata"]
    assert "metadata" in example_a.extra
